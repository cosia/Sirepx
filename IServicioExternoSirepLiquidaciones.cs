﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.IServicioExternoSirepLiquidaciones
// Assembly: AgentesExternos.Sirepx, Version=1.0.4.0, Culture=neutral, PublicKeyToken=null
// MVID: 14F4A259-CD18-4BCA-A219-4DBFA8F3684C
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.dll

using AgentesExternos.Sirepx.Dto.CM;
using AgentesExternos.Sirepx.Dto.ID;
using AgentesExternos.Sirepx.Dto.MEC;
using AgentesExternos.Sirepx.Dto.MPN;

namespace AgentesExternos.Sirepx
{
  public interface IServicioExternoSirepLiquidaciones
  {
    LiquidarCMOutDTO LiquidarCM(LiquidarCMDTO dto);

    LiquidarIDOutDTO LiquidarID(LiquidarIDDTO dto);

    LiquidarMECOutDTO LiquidarMEC(LiquidarMECDTO dto);

    LiquidarMPNOutDTO LiquidarMPN(LiquidarMPNDTO dto);
  }
}
