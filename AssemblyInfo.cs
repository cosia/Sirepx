﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("AgentesExternos.Sirepx")]
[assembly: AssemblyDescription("Contratos de agente de servicios de externos de SIREPX")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("SCI Software")]
[assembly: AssemblyProduct("AgentesExternos.Sirepx")]
[assembly: AssemblyCopyright("Copyright © SCI Software 2014")]
[assembly: AssemblyTrademark("SCI Software")]
[assembly: ComVisible(false)]
[assembly: Guid("f5a530ff-b698-4830-8649-df641f104c24")]
[assembly: AssemblyFileVersion("1.0.4.0")]
[assembly: AssemblyVersion("1.0.4.0")]
