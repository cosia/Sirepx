﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Dto.DetalleLiquidacionDTO
// Assembly: AgentesExternos.Sirepx, Version=1.0.4.0, Culture=neutral, PublicKeyToken=null
// MVID: 14F4A259-CD18-4BCA-A219-4DBFA8F3684C
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.dll

using System;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Dto
{
  [XmlRoot("DetalleLiquidacionDto")]
  [Serializable]
  public class DetalleLiquidacionDTO
  {
    public string ActoId { get; set; }

    public string Cantidad { get; set; }

    public string GastoAdministrativo { get; set; }

    public string Item { get; set; }

    public string ItemPadre { get; set; }

    public string LibroId { get; set; }

    public string Matricula { get; set; }

    public string Servicio { get; set; }

    public string ServicioId { get; set; }

    public string ServicioPadreId { get; set; }

    public string ValorBase { get; set; }

    public Decimal? ValorTotal { get; set; }
  }
}
