﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Dto.ErrorSirepxDto
// Assembly: AgentesExternos.Sirepx, Version=1.0.4.0, Culture=neutral, PublicKeyToken=null
// MVID: 14F4A259-CD18-4BCA-A219-4DBFA8F3684C
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.dll

namespace AgentesExternos.Sirepx.Dto
{
  public class ErrorSirepxDto
  {
    public string ApplicationCode { get; set; }

    public string Criticality { get; set; }

    public string CurrentTime { get; set; }

    public string ErrorCode { get; set; }

    public string Message { get; set; }

    public string ServerName { get; set; }
  }
}
