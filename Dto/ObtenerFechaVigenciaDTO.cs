﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Dto.ObtenerFechaVigenciaDTO
// Assembly: AgentesExternos.Sirepx, Version=1.0.4.0, Culture=neutral, PublicKeyToken=null
// MVID: 14F4A259-CD18-4BCA-A219-4DBFA8F3684C
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.dll

using System;
using System.Collections.Generic;

namespace AgentesExternos.Sirepx.Dto
{
  public class ObtenerFechaVigenciaDTO
  {
    public IEnumerable<ActoDTO> Actos { get; set; }

    public int CtrEspecial { get; set; }

    public DateTime FechaDocumento { get; set; }

    public string IdOrigenDocumento { get; set; }

    public string IdServicioNegocio { get; set; }
  }
}
