﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Dto.GenerarOrdenPagoDto
// Assembly: AgentesExternos.Sirepx, Version=1.0.4.0, Culture=neutral, PublicKeyToken=null
// MVID: 14F4A259-CD18-4BCA-A219-4DBFA8F3684C
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.dll

namespace AgentesExternos.Sirepx.Dto
{
  public class GenerarOrdenPagoDto
  {
    public LiquidarOutDto DatosLiquidacion { get; set; }

    public string FechaVencimiento { get; set; }

    public int? NumeroClientePago { get; set; }

    public string NumeroOrdenPago { get; set; }

    public string NumeroTramite { get; set; }

    public string Observacion { get; set; }

    public int? Reliquidacion { get; set; }

    public string ServicioNegocioId { get; set; }

    public int? SolicitudBpmId { get; set; }

    public string SucursalId { get; set; }

    public string UsuarioId { get; set; }
  }
}
