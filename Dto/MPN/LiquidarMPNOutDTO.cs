﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Dto.MPN.LiquidarMPNOutDTO
// Assembly: AgentesExternos.Sirepx, Version=1.0.4.0, Culture=neutral, PublicKeyToken=null
// MVID: 14F4A259-CD18-4BCA-A219-4DBFA8F3684C
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.dll

using System;

namespace AgentesExternos.Sirepx.Dto.MPN
{
  public class LiquidarMPNOutDTO
  {
    public string DescMoneda { get; set; }

    public string Detalle { get; set; }

    public string IdMoneda { get; set; }

    public LiquidacionSirepMPNDTO LiquidacionSirep { get; set; }

    public string MensajeError { get; set; }

    public Decimal TotalOperacion { get; set; }
  }
}
