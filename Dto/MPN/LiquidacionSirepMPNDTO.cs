﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Dto.MPN.LiquidacionSirepMPNDTO
// Assembly: AgentesExternos.Sirepx, Version=1.0.4.0, Culture=neutral, PublicKeyToken=null
// MVID: 14F4A259-CD18-4BCA-A219-4DBFA8F3684C
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.dll

using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Dto.MPN
{
  [XmlRoot("Liquidacion")]
  [Serializable]
  public class LiquidacionSirepMPNDTO
  {
    [XmlArray("ActosVigencia")]
    [XmlArrayItem("ActoVigencia")]
    public List<ActosVigenciaDTO> ActosVigencia { get; set; }

    [XmlArray("ArrayOfDetalleLiquidacionDto")]
    [XmlArrayItem("DetalleLiquidacionDto")]
    public List<DetalleLiquidacionDTO> DetallesLiquidacion { get; set; }

    [XmlElement("NumMatricula")]
    public string Matricula { get; set; }

    public string Moneda { get; set; }

    public Decimal ValorTotal { get; set; }
  }
}
