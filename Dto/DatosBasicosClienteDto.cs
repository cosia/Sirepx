﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Dto.DatosBasicosClienteDto
// Assembly: AgentesExternos.Sirepx, Version=1.0.4.0, Culture=neutral, PublicKeyToken=null
// MVID: 14F4A259-CD18-4BCA-A219-4DBFA8F3684C
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.dll

namespace AgentesExternos.Sirepx.Dto
{
  public class DatosBasicosClienteDto
  {
    public int? NumeroCliente { get; set; }

    public string NumeroIdentificacion { get; set; }

    public string TipoIdentificacion { get; set; }
  }
}
