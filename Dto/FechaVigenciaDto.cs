﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Dto.FechaVigenciaDto
// Assembly: AgentesExternos.Sirepx, Version=1.0.4.0, Culture=neutral, PublicKeyToken=null
// MVID: 14F4A259-CD18-4BCA-A219-4DBFA8F3684C
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.dll

using System;

namespace AgentesExternos.Sirepx.Dto
{
  public class FechaVigenciaDto
  {
    public string CodigoError { get; set; }

    public DateTime FechaVigencia { get; set; }

    public string MensajeError { get; set; }
  }
}
