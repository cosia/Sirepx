﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Dto.ActosVigenciaDTO
// Assembly: AgentesExternos.Sirepx, Version=1.0.4.0, Culture=neutral, PublicKeyToken=null
// MVID: 14F4A259-CD18-4BCA-A219-4DBFA8F3684C
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.dll

using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Dto
{
  [XmlRoot("ActoVigencia")]
  [Serializable]
  public class ActosVigenciaDTO
  {
    [XmlElement("FechaDocumentobroId")]
    public string FechaDocumentoId { get; set; }

    public string IndicadorEspecial { get; set; }

    [XmlElement("OrigenIdDocumento")]
    public string OrigenDocumentoId { get; set; }

    [XmlArray("Actos")]
    [XmlArrayItem("Acto")]
    public List<ActoDTO> Actos { get; set; }
  }
}
