﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Dto.RegistrarPagoIDDto
// Assembly: AgentesExternos.Sirepx, Version=1.0.4.0, Culture=neutral, PublicKeyToken=null
// MVID: 14F4A259-CD18-4BCA-A219-4DBFA8F3684C
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.dll

namespace AgentesExternos.Sirepx.Dto
{
  public class RegistrarPagoIDDto
  {
    public string BancoId { get; set; }

    public string FechaRecuado { get; set; }

    public string FormaPagoId { get; set; }

    public string MonedaId { get; set; }

    public string NumeroAutorizacion { get; set; }

    public string NumeroClientePago { get; set; }

    public string NumeroDocumento { get; set; }

    public string NumeroSolicitud { get; set; }

    public string OrdenPago { get; set; }

    public string TipoTarjetaId { get; set; }

    public string ValorPago { get; set; }
  }
}
