﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Dto.OrdenPagoDto
// Assembly: AgentesExternos.Sirepx, Version=1.0.4.0, Culture=neutral, PublicKeyToken=null
// MVID: 14F4A259-CD18-4BCA-A219-4DBFA8F3684C
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.dll

using System;
using System.Collections.Generic;

namespace AgentesExternos.Sirepx.Dto
{
  public class OrdenPagoDto
  {
    public ObtenerFechaVigenciaDTO ActosVigencia { get; set; }

    public string IdMoneda { get; set; }

    public string IdServicioNegocio { get; set; }

    public string IdSucursal { get; set; }

    public string IdUsuario { get; set; }

    public IEnumerable<OrdenPagoServiciosDto> LiquidacionServicios { get; set; }

    public string NomMoneda { get; set; }

    public int NumCliente { get; set; }

    public Decimal VrTotal { get; set; }
  }
}
