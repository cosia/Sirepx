﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Dto.LiquidacionDto
// Assembly: AgentesExternos.Sirepx, Version=1.0.4.0, Culture=neutral, PublicKeyToken=null
// MVID: 14F4A259-CD18-4BCA-A219-4DBFA8F3684C
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.dll

using System;

namespace AgentesExternos.Sirepx.Dto
{
  public class LiquidacionDto
  {
    public string AnoRenova { get; set; }

    public int Cantidad { get; set; }

    public int? CntPersonal { get; set; }

    public string FechaBase { get; set; }

    public string FechaEspecial { get; set; }

    public Decimal GastoAdministrativo { get; set; }

    public string IdActo { get; set; }

    public int? IdCertificado { get; set; }

    public int? IdDescuento { get; set; }

    public int? IdFondo { get; set; }

    public string IdGastoAdmin { get; set; }

    public string IdLibro { get; set; }

    public string IdServicio { get; set; }

    public string IdServicioBase { get; set; }

    public string IdServicioPadre { get; set; }

    public string IdTipoVenta { get; set; }

    public int Item { get; set; }

    public int ItemPadre { get; set; }

    public string NombreMatricula { get; set; }

    public string NombreServicio { get; set; }

    public string NumMatricula { get; set; }

    public string NumNotaCredito { get; set; }

    public string NumRecibo { get; set; }

    public string NumTramite { get; set; }

    public Decimal ValorActivoTotal { get; set; }

    public Decimal ValorBase { get; set; }

    public Decimal ValorServicio { get; set; }
  }
}
