﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Dto.ID.RegistrarPagoDTO
// Assembly: AgentesExternos.Sirepx, Version=1.0.4.0, Culture=neutral, PublicKeyToken=null
// MVID: 14F4A259-CD18-4BCA-A219-4DBFA8F3684C
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.dll

using System;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Dto.ID
{
  [XmlRoot("registrarPagoIDCCB")]
  [Serializable]
  public class RegistrarPagoDTO
  {
    [XmlElement("metadata")]
    public MetadataDTO Metadata { get; set; }

    [XmlElement("data")]
    public DataDTO Data { get; set; }
  }
}
