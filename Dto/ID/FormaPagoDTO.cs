﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Dto.ID.FormaPagoDTO
// Assembly: AgentesExternos.Sirepx, Version=1.0.4.0, Culture=neutral, PublicKeyToken=null
// MVID: 14F4A259-CD18-4BCA-A219-4DBFA8F3684C
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.dll

using System;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Dto.ID
{
  [XmlRoot("formaPago")]
  [Serializable]
  public class FormaPagoDTO
  {
    [XmlElement("idBanco")]
    public string BancoId { get; set; }

    [XmlElement("idFormaPago")]
    public string FormaPagoId { get; set; }

    [XmlElement("idMoneda")]
    public string MonedaId { get; set; }

    [XmlElement("numAutoriza")]
    public string NumeroAutorizacion { get; set; }

    [XmlElement("numDocumento")]
    public string NumeroDocumento { get; set; }

    [XmlElement("idTipoTarjeta")]
    public string TipoTarjetaId { get; set; }

    [XmlElement("valorPago")]
    public Decimal ValorPago { get; set; }
  }
}
