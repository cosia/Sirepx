﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Dto.ID.MetadataDTO
// Assembly: AgentesExternos.Sirepx, Version=1.0.4.0, Culture=neutral, PublicKeyToken=null
// MVID: 14F4A259-CD18-4BCA-A219-4DBFA8F3684C
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.dll

using System;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Dto.ID
{
  [XmlRoot("metadata")]
  [Serializable]
  public class MetadataDTO
  {
    [XmlElement("transactionID")]
    public string TransactionID { get; set; }

    [XmlElement("version")]
    public string Version { get; set; }
  }
}
