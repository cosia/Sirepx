﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Dto.ID.RegistrarPagoInDTO
// Assembly: AgentesExternos.Sirepx, Version=1.0.4.0, Culture=neutral, PublicKeyToken=null
// MVID: 14F4A259-CD18-4BCA-A219-4DBFA8F3684C
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.dll

using System;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Dto.ID
{
  [XmlRoot("registrarPagoInDTO")]
  [Serializable]
  public class RegistrarPagoInDTO
  {
    [XmlElement("fecRecaudo")]
    public string FechaRecaudo { get; set; }

    [XmlElement("formaPago")]
    public FormaPagoDTO FormaPago { get; set; }

    [XmlElement("numClientePago")]
    public int NumeroClientePago { get; set; }

    [XmlElement("numRecibo")]
    public string NumeroRecibo { get; set; }

    [XmlElement("numSolicitud")]
    public string NumeroSolicitud { get; set; }

    [XmlElement("numOrdenPago")]
    public string OrdenPago { get; set; }
  }
}
