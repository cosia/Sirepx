﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Dto.OrdenPagoServiciosDto
// Assembly: AgentesExternos.Sirepx, Version=1.0.4.0, Culture=neutral, PublicKeyToken=null
// MVID: 14F4A259-CD18-4BCA-A219-4DBFA8F3684C
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.dll

using System;

namespace AgentesExternos.Sirepx.Dto
{
  public class OrdenPagoServiciosDto
  {
    public int CntPersonal { get; set; }

    public int CntServicio { get; set; }

    public DateTime FechaBase { get; set; }

    public string IdActo { get; set; }

    public string IdLibro { get; set; }

    public string IdServicio { get; set; }

    public string IdServicioBase { get; set; }

    public int Item { get; set; }

    public int ItemPadre { get; set; }

    public string NombreServicio { get; set; }

    public string NumMatricula { get; set; }

    public Decimal? VrBase { get; set; }

    public Decimal VrServicio { get; set; }
  }
}
