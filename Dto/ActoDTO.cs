﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Dto.ActoDTO
// Assembly: AgentesExternos.Sirepx, Version=1.0.4.0, Culture=neutral, PublicKeyToken=null
// MVID: 14F4A259-CD18-4BCA-A219-4DBFA8F3684C
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.dll

using System;
using System.Xml.Serialization;

namespace AgentesExternos.Sirepx.Dto
{
  [XmlRoot("Acto")]
  [Serializable]
  public class ActoDTO
  {
    public string ActoId { get; set; }

    public string ImpuestoId { get; set; }

    public string LibroId { get; set; }
  }
}
