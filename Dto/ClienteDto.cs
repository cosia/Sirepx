﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.Dto.ClienteDto
// Assembly: AgentesExternos.Sirepx, Version=1.0.4.0, Culture=neutral, PublicKeyToken=null
// MVID: 14F4A259-CD18-4BCA-A219-4DBFA8F3684C
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.dll

namespace AgentesExternos.Sirepx.Dto
{
  public class ClienteDto
  {
    public string Apellidos { get; set; }

    public string Ciudad { get; set; }

    public string CiudadId { get; set; }

    public string Correo { get; set; }

    public string Departamento { get; set; }

    public string Direccion { get; set; }

    public string Identificacion { get; set; }

    public string Nombres { get; set; }

    public string Pais { get; set; }

    public string PaisId { get; set; }

    public string Telefono { get; set; }

    public int TipoIdentificacion { get; set; }
  }
}
