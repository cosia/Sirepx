﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.ExcepcionSirepx
// Assembly: AgentesExternos.Sirepx, Version=1.0.4.0, Culture=neutral, PublicKeyToken=null
// MVID: 14F4A259-CD18-4BCA-A219-4DBFA8F3684C
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.dll

using AgentesExternos.Sirepx.Dto;
using System;

namespace AgentesExternos.Sirepx
{
  public class ExcepcionSirepx : Exception
  {
    private readonly ErrorSirepxDto _errorSirepx;

    public ExcepcionSirepx(ErrorSirepxDto errorSirepx)
      : base(errorSirepx.Message)
    {
      this._errorSirepx = errorSirepx;
    }

    public ErrorSirepxDto ErrorSirepx
    {
      get
      {
        return this._errorSirepx;
      }
    }
  }
}
