﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.IServicioExternoSirepxRecaudos
// Assembly: AgentesExternos.Sirepx, Version=1.0.4.0, Culture=neutral, PublicKeyToken=null
// MVID: 14F4A259-CD18-4BCA-A219-4DBFA8F3684C
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.dll

using AgentesExternos.Sirepx.Dto;
using System.Threading.Tasks;

namespace AgentesExternos.Sirepx
{
  public interface IServicioExternoSirepxRecaudos
  {
    PagoVirtualDto GenerarOrdenDePagoVirtual(OrdenPagoDto ordenPagoDto);

    Task<PagoVirtualDto> GenerarOrdenDePagoVirtualAsync(OrdenPagoDto ordenPagoDto);
  }
}
