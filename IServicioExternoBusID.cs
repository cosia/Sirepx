﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.Sirepx.IServicioExternoBusID
// Assembly: AgentesExternos.Sirepx, Version=1.0.4.0, Culture=neutral, PublicKeyToken=null
// MVID: 14F4A259-CD18-4BCA-A219-4DBFA8F3684C
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.Sirepx.dll

using AgentesExternos.Sirepx.Dto;

namespace AgentesExternos.Sirepx
{
  public interface IServicioExternoBusID
  {
    RegistroPagoVirtualDto RegistrarPago(RegistrarPagoIDDto registrarPagoIdDto);
  }
}
